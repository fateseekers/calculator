package com.example.calculator3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Integer.parseInt
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import net.objecthunter.exp4j.ExpressionBuilder
import java.lang.Integer.toString
import kotlin.ArithmeticException


class MainActivity : AppCompatActivity() {

    lateinit var tVResult: TextView;
    lateinit var tVString: TextView;
    var lastNumeric: Boolean = false;
    var stateError: Boolean = false;
    var lastDot: Boolean = false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tVResult = findViewById<TextView>(R.id.TVResult)
        tVString = findViewById<TextView>(R.id.TVString)
    }

    fun onNumberClick(view: View){
        if(stateError){
            tVString.text = (view as Button).text;
            stateError = false
        } else {
            tVString.append((view as Button).text);
        }

        lastNumeric = true
    }

    fun onDotClick(view: View){
        if(lastNumeric && !stateError){
            tVString.append(".");
            lastNumeric = false;
            lastDot = true
        }
    }

    fun onOperatorClick(view: View){
        if(lastNumeric && !stateError){
            tVString.append((view as Button).text);
            lastNumeric = false;
            lastDot = false;
        }
    }

    fun onClear(view: View){
        TVString.text = "";
        TVResult.text = "";
        lastNumeric = false;
        lastDot = false;
        stateError = false
    }

    fun onEqual(view: View){
        if(lastNumeric && !stateError){
            val txt = tVString.text.toString();
            val expression = ExpressionBuilder(txt).build();

            try{
                val result = expression.evaluate();
                tVResult.text = result.toString();
                lastDot = true
            } catch (ex: ArithmeticException){
                tVResult.text = "ERROR"
                stateError = true
                lastNumeric = false
            }
        }
    }

    fun onBackspaceClick(view: View) {
        if(!stateError) {
            val txt = tVString.text.dropLast(1);
            tVString.text = txt
        }
    }
}



